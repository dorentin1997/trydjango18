# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0006_person_created_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='description',
            field=models.CharField(max_length=400, blank=True),
        ),
    ]
