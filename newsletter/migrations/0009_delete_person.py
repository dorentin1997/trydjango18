# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0008_remove_person_description'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Person',
        ),
    ]
