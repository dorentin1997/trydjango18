# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0005_auto_20151017_0829'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 25, 18, 50, 1, 812064, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
