from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from cslist.views import cslist

urlpatterns = [
    # Examples:
     url(r'^$', 'newsletter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^contact/$', 'newsletter.views.contact', name='contact'),
    url(r'^about/$', 'trydjango18.views.about', name='about'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^cslist/', cslist, name="cslist"),
    # url(r'^information/$', 'newsletter.views.information', name='information'),
] 

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
