from django.shortcuts import render
from .models import CSResource

# Create your views here.
def cslist(request):
    return render(request, "cslist.html", {"cslist": CSResource.objects.all()})
